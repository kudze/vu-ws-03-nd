FROM ubuntu:20.04

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y tzdata \
    && apt-get install -y apache2

RUN a2enmod rewrite

WORKDIR /var/www/html
RUN rm /var/www/html/index.html

RUN apt-get install -y software-properties-common
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt-get update
RUN apt-get install -y \
    php8.1 \
    php8.1-common \
    php8.1-zip \
    php8.1-pdo \
    php8.1-mysql \
    php8.1-curl \
    php8.1-dom \
    php8.1-soap

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY composer.json composer.json
COPY composer.lock composer.lock
RUN composer update

COPY . .
COPY httpd/httpd.conf /etc/apache2/sites-available/000-default.conf
COPY .env.example .env

RUN php artisan wsdl:compile
RUN chown -R www-data:www-data .

EXPOSE 80
CMD apachectl -D FOREGROUND
