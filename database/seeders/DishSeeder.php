<?php

namespace Database\Seeders;

use App\Models\Dish;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for($i = 0; $i < 100; $i++)
        {
            Dish::query()->create(
                [
                    'title' => $faker->words($faker->numberBetween(1, 5), true),
                    'calories' => $faker->numberBetween(1, 1000),
                    'user_id' => null
                ]
            );
        }
    }
}
