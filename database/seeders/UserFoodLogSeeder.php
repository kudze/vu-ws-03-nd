<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserFoodLog;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserFoodLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $id = User::query()->where('username', 'kudze')->first()->getKey();

        for($i = 0; $i < 100; $i++) {
            UserFoodLog::query()->create(
                [
                    'dish_id' => $faker->numberBetween(1, 100),
                    'user_id' => $id,
                    'eaten_at' => $faker->dateTimeBetween('-1 year')->format('Y-m-d H:i:s'),
                ]
            );
        }
    }
}
