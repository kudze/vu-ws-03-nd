<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserCar;
use App\Services\CarRegistryAPIService;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserCarSeeder extends Seeder
{
    private CarRegistryAPIService $carRegistryAPIService;

    public function __construct(CarRegistryAPIService $carRegistryAPIService)
    {
        $this->carRegistryAPIService = $carRegistryAPIService;
    }

    public function run()
    {
        $faker = Factory::create();
        $user_id = User::query()->where('username', 'kudze')->first()->getKey();
        $carRegistryIds = $this->carRegistryAPIService->findSomeUsableIds();

        if (empty($carRegistryIds)) {
            $this->command->error("Cannot seed cars, registry is empty!");
            return;
        }

        for ($i = 0; $i < 50; $i++)
            UserCar::query()->create(
                [
                    'user_id' => $user_id,
                    'license_plate' => $faker->unique()->lexify('???' . $faker->numberBetween(100, 999)),
                    'car_registry_id' => $faker->randomElement($carRegistryIds)
                ]
            );
    }

}
