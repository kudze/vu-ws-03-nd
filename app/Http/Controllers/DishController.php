<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class DishController extends Controller
{
    const MODEL_RULES = [
        'title' => 'required|string|min:3|max:255',
        'calories' => 'required|integer|min:0',
    ];

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    public function index(Request $request): JsonResponse
    {
        [
            'page' => $page,
            'size' => $pageSize
        ] = $this->fetchPaginationInput($request);

        $dishes = Dish::query()->limit($pageSize)->offset($page * $pageSize)->get();
        return response()->json($dishes->toArray());
    }

    public function myIndex(Request $request): JsonResponse
    {
        [
            'page' => $page,
            'size' => $pageSize
        ] = $this->fetchPaginationInput($request);

        /** @var User $user */
        $user = Auth::user();

        $dishes = Dish::query()->where('user_id', $user->getKey())
            ->limit($pageSize)->offset($page * $pageSize)->get();

        return response()->json($dishes->toArray());
    }

    public function get(int $id): JsonResponse
    {
        $dish = Dish::query()->where('id', $id)->first();

        if ($dish === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        return response()->json($dish);
    }

    public function create(Request $request): JsonResponse
    {
        $data = $this->validateModelData($request);

        /** @var User $user */
        $user = Auth::user();
        $data['user_id'] = $user->getKey();
        $dish = Dish::query()->create($data);

        return response()->json($dish->toArray());
    }

    public function update(int $id, Request $request): Response|ResponseFactory|JsonResponse
    {
        $data = $this->validateModelData($request);
        $dish = Dish::query()->where('id', $id)->first();

        if ($dish === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        if (!$this->hasClientPermissionOverModel($dish))
            return $this->unauthorizedResponse();

        $dish->update($data);
        return response()->json($dish->toArray());
    }

    public function delete(int $id): Response|ResponseFactory
    {
        /** @var Dish $dish */
        $dish = Dish::query()->withCount(['logs'])->where('id', $id)->first();

        if ($dish === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        if (!$this->hasClientPermissionOverModel($dish))
            return $this->unauthorizedResponse();

        if ($dish->getAttribute('logs_count') !== 0)
            throw ValidationException::withMessages(
                ['id' => "You first must remove all logs having reference to this dish!"]
            );

        $dish->deleteOrFail();

        return $this->okResponse();
    }

    protected function hasClientPermissionOverModel(Dish $dish): bool
    {
        return self::hasUserPermissionOverModel($dish, Auth::user());
    }

    public static function hasUserPermissionOverModel(Dish $dish, User $user): bool
    {
        return $dish->getAttribute('user_id') === $user->getKey() || $user->getAttribute('role') === 'admin';

    }

    protected function validateModelData(Request $request, array $rules = [])
    {
        return $this->validate($request,
            array_merge(
                self::MODEL_RULES,
                $rules
            )
        );
    }
}
