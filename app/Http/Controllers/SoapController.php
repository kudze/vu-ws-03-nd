<?php

namespace App\Http\Controllers;

use App\Services\Soap\SoapService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use SoapServer;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class SoapController extends Controller
{
    protected function getWSDL(): string
    {
        $result = file_get_contents(__DIR__ . "/../../../food-log.wsdl");;

        if($result === false)
            return "You should run php artisan wsdl-compile";

        return $result;
    }

    public function soap(Request $request, SoapService $service): Response
    {
        if($request->query->has('wsdl'))
            return response(
                $this->getWSDL(),
                200, [
                'Content-Type' => 'application/xml'
            ]);

        if($request->method() === "GET")
            throw new MethodNotAllowedHttpException(['POST']);

        $server = new SoapServer(__DIR__ . "/../../../food-log.wsdl");
        $server->setObject($service);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $server->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }

}
