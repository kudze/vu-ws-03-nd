<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserFoodLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class FoodLogController extends Controller
{
    public const MODEL_RULES = [
        'dish_id' => 'required|integer|exists:dish,id',
        'eaten_at' => 'required|date_format:Y-m-d H:i:s',
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin', ['only' => ['index']]);
    }

    public function index(Request $request): JsonResponse
    {
        [
            'page' => $page,
            'size' => $pageSize
        ] = $this->fetchPaginationInput($request);

        $models = UserFoodLog::query()->limit($pageSize)->offset($page * $pageSize)->get();
        return response()->json($models->toArray());
    }

    public function myIndex(Request $request): JsonResponse
    {
        [
            'page' => $page,
            'size' => $pageSize
        ] = $this->fetchPaginationInput($request);

        /** @var User $user */
        $user = Auth::user();

        $models = UserFoodLog::query()->where('user_id', $user->getKey())
            ->limit($pageSize)->offset($page * $pageSize)->get();

        return response()->json($models->toArray());
    }

    public function get(int $id): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        $query = UserFoodLog::query();
        if ($user->getAttribute('role') !== 'admin')
            $query->where('user_id', $user->getKey());

        $model = $query->where('id', $id)->first();

        if ($model === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        return response()->json($model);
    }

    public function create(Request $request): JsonResponse
    {
        $data = $this->validateModelData($request);

        /** @var User $user */
        $user = Auth::user();
        $data['user_id'] = $user->getKey();

        $model = UserFoodLog::query()->create($data);

        return response()->json($model->toArray());
    }

    public function update(int $id, Request $request): Response|ResponseFactory|JsonResponse
    {
        $data = $this->validateModelData($request);

        /** @var UserFoodLog $model */
        $model = UserFoodLog::query()->find($id);
        if ($model === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        if (!$this->hasClientPermissionOverModel($model))
            return $this->unauthorizedResponse();

        $model->update($data);

        return response()->json($model->toArray());
    }

    public function delete(int $id): Response|ResponseFactory
    {
        /** @var UserFoodLog $model */
        $model = UserFoodLog::query()->find($id);
        if ($model === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        if (!$this->hasClientPermissionOverModel($model))
            return $this->unauthorizedResponse();

        $model->deleteOrFail();

        return $this->okResponse();
    }

    protected function hasClientPermissionOverModel(UserFoodLog $model): bool
    {
        return $model->getAttribute('user_id') === Auth::id() || Auth::user()->getAttribute('role') === 'admin';
    }

    protected function validateModelData(Request $request, array $rules = [])
    {
        return $this->validate($request,
            array_merge(
                self::MODEL_RULES,
                $rules
            )
        );
    }
}
