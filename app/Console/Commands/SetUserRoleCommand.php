<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetUserRoleCommand extends Command
{
    protected $signature = "user:role:set {username} {role}";
    protected $description = "Sets the role of an user!";

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $role = $input->getArgument('role');

        $user = User::query()->where('username', $username)->first();

        if($user === null) {
            $output->writeln("User with username $username does not exist!");
            return self::FAILURE;
        }

        $user->update(['role' => $role]);
        $output->writeln("User with username $username role was updated to: $role");
        return self::SUCCESS;
    }
}
