<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = "user";

    /**
     * Returns the dishes, that user created.
     *
     * @return HasMany
     */
    public function cars(): HasMany
    {
        return $this->hasMany(UserCar::class, 'user_id', 'id');
    }

    public function dishes(): HasMany
    {
        return $this->hasMany(Dish::class, 'user_id', 'id');
    }

    public function tokens(): HasMany
    {
        return $this->hasMany(UserToken::class, 'user_id', 'id');
    }

    public function food_log(): HasMany
    {
        return $this->hasMany(UserFoodLog::class, 'user_id', 'id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id', 'username', 'created_at', 'updated_at', 'password', 'role'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [
        'password',
    ];
}
