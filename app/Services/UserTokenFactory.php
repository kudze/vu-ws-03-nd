<?php

namespace App\Services;

use App\Exceptions\UserTokenCreationFailedException;
use App\Models\User;
use App\Models\UserToken;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class UserTokenFactory
{
    public const TOKEN_LEN = 128;
    public const TOKEN_LIFETIME_MINUTES = 60;

    public const MAX_ITER = 5;

    public function createNewAccessTokenForUser(User $user): UserToken
    {
        for ($i = 0; $i < self::MAX_ITER; $i++) {
            $token = Str::random(self::TOKEN_LEN);

            try {
                /** @var UserToken $tokenModel */
                $tokenModel = UserToken::query()->create(
                    [
                        'user_id' => $user->getKey(),
                        'created_from_ip' => Request::ip(),
                        'valid_till' => Carbon::now()->addMinutes(self::TOKEN_LIFETIME_MINUTES),
                        'token' => $token
                    ]
                );

                return $tokenModel;
            } catch(QueryException) {
                continue;
            }
        }

        throw new UserTokenCreationFailedException();
    }
}
