<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

class CarRegistryAPIService
{
    private const URL = 'http://car_registry_rest_api/';
    private Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getCar(int $id): ?array
    {
        try {
            $response = $this->client->get(self::URL . "cars/$id");
            return json_decode($response->getBody()->getContents(), true);
        } catch (GuzzleException) {
            return null;
        }
    }

    public function createCar(string $brand, string $model, string $country, int $year): ?int
    {
        try {
            $response = $this->client->post(self::URL . 'cars/0', [
                'json' => [
                    'brand' => $brand,
                    'model' => $model,
                    'country' => $country,
                    'year' => $year
                ]
            ]);

            $data = json_decode($response->getBody()->getContents(), true);
            return $data['car_id'];
        } catch (GuzzleException) {
            return null;
        }
    }

    public function updateCar(int $id, string $brand, string $model, string $country, int $year): bool
    {
        try {
            $this->client->put(self::URL . "cars/$id", [
                'json' => [
                    'brand' => $brand,
                    'model' => $model,
                    'country' => $country,
                    'year' => $year
                ]
            ]);
            return true;
        } catch (GuzzleException) {
            return false;
        }
    }

    public function deleteCar(int $id): bool
    {
        try {
            $this->client->delete(self::URL . "cars/$id");
            return true;
        } catch (GuzzleException) {
            return false;
        }
    }

    public function findSomeUsableIds(): array
    {
        $ids = [];

        for($i = 0; $i < 100; $i++)
            if($this->getCar($i) !== null) $ids[] = $i;

        return $ids;
    }

}
