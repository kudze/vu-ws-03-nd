<?php

namespace App\Services\Soap;

use App\Models\UserCar;

class UserCarResponse
{
    public int $id;
    public int $user_id;
    public string $created_at;
    public string $updated_at;
    public string $license_plate;
    public int $car_registry_id;

    public function __construct(UserCar $userCar)
    {
        $this->id = $userCar->getAttribute('id');
        $this->user_id = $userCar->getAttribute('user_id');
        $this->created_at = $userCar->getAttribute('created_at');
        $this->updated_at = $userCar->getAttribute('updated_at') ?? '';
        $this->license_plate = $userCar->getAttribute('license_plate');
        $this->car_registry_id = $userCar->getAttribute('car_registry_id');
    }
}
