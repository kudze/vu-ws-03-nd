<?php

namespace App\Services\Soap;

use App\Models\User;

class UserResponse
{
    public int $id;
    public string $username;
    public string $role;
    public string $created_at;
    public string $updated_at;

    public function __construct(User $user)
    {
        $this->id = $user->getAttribute('id');
        $this->username = $user->getAttribute('username');
        $this->role = $user->getAttribute('role') ?? "default";
        $this->created_at = $user->getAttribute('created_at');
        $this->updated_at = $user->getAttribute('updated_at') ?? "";
    }
}
