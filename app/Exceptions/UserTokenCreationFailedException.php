<?php

namespace App\Exceptions;

use RuntimeException;

class UserTokenCreationFailedException extends RuntimeException
{

}
