<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) { return "Hello World"; });
$router->get('/soap', ['as' => 'soap', 'uses' => 'SoapController@soap']);
$router->post('/soap', ['as' => 'soap', 'uses' => 'SoapController@soap']);

$router->post('/register', ['as' => 'register', 'uses' => 'AuthController@register']);
$router->post('/login', ['as' => 'login', 'uses' => 'AuthController@login']);
$router->post('/token/refresh', ['as' => 'token.refresh', 'uses' => 'AuthController@refresh']);
$router->delete('/me', ['as' => 'user.delete', 'uses' => 'AuthController@delete']);
$router->get('/me', ['as' => 'user.me', 'uses' => 'AuthController@me']);

$router->get('/dishes', ['as' => 'dish.index', 'uses' => 'DishController@index']);
$router->get('/dishes/my', ['as' => 'dish.my.index', 'uses' => 'DishController@myIndex']);
$router->get('/dishes/{id}', ['as' => 'dish.my.get', 'uses' => 'DishController@get']);
$router->post('/dishes', ['as' => 'dish.create', 'uses' => 'DishController@create']);
$router->put('/dishes/{id}', ['as' => 'dish.update', 'uses' => 'DishController@update']);
$router->delete('/dishes/{id}', ['as' => 'dish.delete', 'uses' => 'DishController@delete']);

$router->get('/logs', ['as' => 'dish.index', 'uses' => 'FoodLogController@index']);
$router->get('/logs/my', ['as' => 'dish.my.index', 'uses' => 'FoodLogController@myIndex']);
$router->get('/logs/{id}', ['as' => 'dish.my.get', 'uses' => 'FoodLogController@get']);
$router->post('/logs', ['as' => 'dish.create', 'uses' => 'FoodLogController@create']);
$router->put('/logs/{id}', ['as' => 'dish.update', 'uses' => 'FoodLogController@update']);
$router->delete('/logs/{id}', ['as' => 'dish.delete', 'uses' => 'FoodLogController@delete']);

$router->get('/cars', ['as' => 'cars.index', 'uses' => 'UserCarController@index']);
$router->get('/cars/my', ['as' => 'cars.my.index', 'uses' => 'UserCarController@myIndex']);
$router->get('/cars/{id}', ['as' => 'cars.my.get', 'uses' => 'UserCarController@get']);
$router->post('/cars', ['as' => 'cars.create', 'uses' => 'UserCarController@create']);
$router->put('/cars/{id}', ['as' => 'cars.create', 'uses' => 'UserCarController@update']);
$router->delete('/cars/{id}', ['as' => 'cars.delete', 'uses' => 'UserCarController@delete']);

$router->get('/stats/calories', ['as' => 'dish.index', 'uses' => 'StatisticsController@totalMyCalories']);
$router->get('/stats/counts', ['as' => 'dish.index', 'uses' => 'StatisticsController@countMyDishes']);
$router->get('/stats/cars_count', ['as' => 'dish.index', 'uses' => 'StatisticsController@totalCars']);

