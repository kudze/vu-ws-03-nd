# Karolis Kraujelis Web Servizai 01 Laboratorinis

## Aprašymas

Servizas yra labai paprastas, yra varototojai kurie gali supildyti patiekalų sarašą ir jų vartojimo istorijos sąrašą.
Tada pagal įvestus duomenis galime paskaičiuoti įvairią statistiką.

Pagal antrą labaratorinį panaudojau CarRegistryRestAPI servizą: https://github.com/RafalKLab/CarRegistryRestApi
Vartotojai prie sistemos gali pridėti savo automobilius, ir prie statistikos paskaičiuoti jų skaičių.

## Įrašymas

Norint pasileisti reikia:

1. `git clone https://gitlab.com/kudze/vu-ws-01-nd.git` - Nuklonuoja git repositoriją.
2. `cd vu-ws-01-nd` - Ieiname i atsiusta direktorija.
3. `git submodule init` - Sukurs tuscia git repositorija, kur bus kito studento panaudotas darbas.
4. `git submodule update` - Atpullins commita git repozitorijos, kur yra kito studento panaudotas darbas.
5. `docker-compose up -d` - Paleidžia visus reikiamus servizus. Bus sukurtas admin user'is su prisijungimais (kudze : test)

## Dependencies

* Docker
* Docker-Compose
* Git

## WSDL

WSDL paleidus servizus bus pasiekiamas adresu `http://localhost/soap?wsdl`

## Užklausų pavyzdžiai

### Prisijungimas

```
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://localhost/soap?wsdl">
   <soap:Header/>
   <soap:Body>
      <soap1:usernameLogin xsi:type="xsd:string">kudze</soap1:usernameLogin>
      <soap1:passwordLogin xsi:type="xsd:string">temp</soap1:passwordLogin>
   </soap:Body>
</soap:Envelope>
```

Čia reikia išsisaugoti gražinamą tokeną, kurį reikia pridėti prie kitų requestų.

Pavyzdžiui:

1. Užklausa gražino:
```
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://localhost/soap?wsdl" xmlns:enc="http://www.w3.org/2003/05/soap-encoding">
<env:Body>
    <ns1:userTokenFromLogin xsi:type="ns1:UserTokenFromLogin">
        <token xsi:type="xsd:string">0hocwxldNKdIypeI5AiSPqM9DEOWrI0pDo3vKk8z67e9pm4Z8Priklm43k2IuM30KL3zbV9ttBDsxzTP2LNmv6zQ53yv67lY5mcqJ2ZsuAY5TdIAtw6YqCBvL7T1pmLi</token>
            <created_from_ip xsi:type="xsd:string">172.21.0.1</created_from_ip>
            <valid_till xsi:type="xsd:string">2022-05-17 04:05:59</valid_till>
        </ns1:userTokenFromLogin>
    </env:Body>
/env:Envelope>
```

2. Tada tokenas bus: `0hocwxldNKdIypeI5AiSPqM9DEOWrI0pDo3vKk8z67e9pm4Z8Priklm43k2IuM30KL3zbV9ttBDsxzTP2LNmv6zQ53yv67lY5mcqJ2ZsuAY5TdIAtw6YqCBvL7T1pmLi`

### Pirmu dvieju mašinu peržiurejimas

```
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://localhost/soap?wsdl">
   <soap:Header/>
   <soap:Body>
      <soap1:myCarsPage xsi:type="xsd:int">0</soap1:myCarsPage>
      <soap1:myCarsPageSize xsi:type="xsd:int">2</soap1:myCarsPageSize>
      <soap1:myTokenCarsList xsi:type="xsd:string">0hocwxldNKdIypeI5AiSPqM9DEOWrI0pDo3vKk8z67e9pm4Z8Priklm43k2IuM30KL3zbV9ttBDsxzTP2LNmv6zQ53yv67lY5mcqJ2ZsuAY5TdIAtw6YqCBvL7T1pmLi</soap1:myTokenCarsList>
   </soap:Body>
</soap:Envelope>
```

### Mašinos Sukurimas

Paprastas:
```
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://localhost/soap?wsdl">
   <soap:Header/>
   <soap:Body>
      <soap1:carRegistryIdCreateCar xsi:type="xsd:int">11</soap1:carRegistryIdCreateCar>
      <soap1:licensePlateCreateCar xsi:type="xsd:string">POR533</soap1:licensePlateCreateCar>
      <soap1:tokenCreateCar xsi:type="xsd:string">0hocwxldNKdIypeI5AiSPqM9DEOWrI0pDo3vKk8z67e9pm4Z8Priklm43k2IuM30KL3zbV9ttBDsxzTP2LNmv6zQ53yv67lY5mcqJ2ZsuAY5TdIAtw6YqCBvL7T1pmLi</soap1:tokenCreateCar>
   </soap:Body>
</soap:Envelope>
```

Kompozicinis:
```
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://localhost/soap?wsdl">
   <soap:Header/>
   <soap:Body>
      <soap1:licensePlateCreateCarComposed xsi:type="xsd:string">JOB528</soap1:licensePlateCreateCarComposed>
      <soap1:brandCreateCarComposed xsi:type="xsd:string">BMW</soap1:brandCreateCarComposed>
      <soap1:modelCreateCarComposed xsi:type="xsd:string">E61</soap1:modelCreateCarComposed>
      <soap1:countryCreateCarComposed xsi:type="xsd:string">Vokietija</soap1:countryCreateCarComposed>
      <soap1:yearCreateCarComposed xsi:type="xsd:int">2004</soap1:yearCreateCarComposed>
      <soap1:tokenCreateCarComposed xsi:type="xsd:string">0hocwxldNKdIypeI5AiSPqM9DEOWrI0pDo3vKk8z67e9pm4Z8Priklm43k2IuM30KL3zbV9ttBDsxzTP2LNmv6zQ53yv67lY5mcqJ2ZsuAY5TdIAtw6YqCBvL7T1pmLi</soap1:tokenCreateCarComposed>
   </soap:Body>
</soap:Envelope>
```

### Mašinos peržiurėjimas:
```
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://localhost/soap?wsdl">
   <soap:Header/>
   <soap:Body>
      <soap1:carIdToFetch xsi:type="xsd:int">1</soap1:carIdToFetch>
      <soap1:tokenGetCar xsi:type="xsd:string">0hocwxldNKdIypeI5AiSPqM9DEOWrI0pDo3vKk8z67e9pm4Z8Priklm43k2IuM30KL3zbV9ttBDsxzTP2LNmv6zQ53yv67lY5mcqJ2ZsuAY5TdIAtw6YqCBvL7T1pmLi</soap1:tokenGetCar>
   </soap:Body>
</soap:Envelope>
```

